from django.db import models

# Create your models here.
class Key(models.Model):
    title = models.CharField(max_length=200)
    urls = models.CharField(max_length=100)
    ##answer = models.CharField(max_length=10000000)

    def __str__(self):
        return self.title