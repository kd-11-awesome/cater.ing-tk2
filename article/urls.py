from django.urls import path
from . import views

app_name = 'article'

urlpatterns = [
    path('', views.article, name='article'),
    path('article-1/', views.artikelsatu, name='artikelsatu'),
    path('article-2/', views.artikeldua, name='artikeldua'),
]