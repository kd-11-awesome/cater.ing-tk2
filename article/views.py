from django.shortcuts import render

# Create your views here.
def article(request):
    return render(request, 'katering_article.html')

def artikelsatu(request):
    return render(request, 'article_one.html')  

def artikeldua(request):
    return render(request, 'article_two.html')    

def search(request):
    query = request.GET.get()
    if query:
        # query example
        results = Key.objects.filter(name__contains=query)
    else:
        results = []
    return render(request, katering_article.html, {'results': results})
