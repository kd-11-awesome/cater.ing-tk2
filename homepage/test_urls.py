from django.test import TestCase
from django.test import SimpleTestCase
from django.urls import resolve
from homepage.views import homepage, qna


# Create your tests here.

class TestUrl(SimpleTestCase):

    def test_homepage_is_exist(self):
        url = resolve('/')
        self.assertEqual(url.func, homepage)

    def test_qna_is_exist(self):
        url = resolve('/qna/')
        self.assertEqual(url.func, qna)
