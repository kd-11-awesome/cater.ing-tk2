from django.test import TestCase, Client
from django.urls import reverse
from homepage.models import Answer


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.qna = reverse('homepage:qna')
    
    def test_qna_GET(self):
        response = self.client.get(self.qna)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'QnA-page.html')

    def test_qna_POST(self):
        Answer.objects.create(
            key='pesan',
            answer='bisa hubungi kami di +62-812-9128-0150'
            )
        Answer.objects.create(
            key='notfound',
            answer=':(('
            )
        response = self.client.post(self.qna, {'keyword':'pesan'})

        self.assertEquals(response.status_code, 200)
