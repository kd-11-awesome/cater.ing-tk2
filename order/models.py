from django.db import models

# Create your models here.

class FormPemesanan(models.Model):
	namaPemesan = models.CharField(max_length = 50)
	pilihanMenu = models.CharField(max_length = 500)
	alamatPengiriman = models.CharField(max_length = 80)
	jenisPembayaran = models.CharField(max_length = 30)
	nomorTelepon = models.IntegerField(default=0)
	notes = models.TextField()

	def __str__(self):
		return self.namaPemesan
