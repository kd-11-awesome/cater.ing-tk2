from django.urls import path
from . import views

app_name = 'order'

urlpatterns = [
    path('', views.form_order, name = 'form_order')
]