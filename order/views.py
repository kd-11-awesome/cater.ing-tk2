from django.shortcuts import render
from .models import FormPemesanan

# Create your views here.
def form_order(request):

	if request.method == 'POST':

		nama_pemesan = request.POST.get('nama')
		pilihan_menu = request.POST.get('menu')
		alamat = request.POST.get('alamat')
		pembayaran = request.POST.get('pembayaran')
		no_telp = request.POST.get('telepon')
		notes = request.POST.get('notes')

		form_pemesanan = FormPemesanan(namaPemesan = nama_pemesan, pilihanMenu = pilihan_menu, alamatPengiriman = alamat, jenisPembayaran = pembayaran, nomorTelepon = no_telp, notes = notes)
		form_pemesanan.save()

		ucapan_terima_kasih = 'Terima kasih telah mengisi form ini ya!'
		terimakasih = [ucapan_terima_kasih]

		return render(request, 'Form Pemesanan.html', {'terimakasih' : ''})

	else:
		return render(request, 'Form Pemesanan.html')